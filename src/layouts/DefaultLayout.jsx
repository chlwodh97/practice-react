import React from "react";
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <header>
                <nev>
                    <p><Link to="/">이름 궁합도 보기</Link></p>
                    <p><Link to="/t2">상하의 랜덤 정해주기</Link></p>
                    <p><Link to="/t3">랜덤박스 열기</Link></p>
                    <p><Link to="/t4">볼불복 N빵</Link></p>
                </nev>
            </header>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;