import React, {useState} from 'react';

const Test4 = () =>{
    const [allMoney, setAllMoney] = useState('')

    const [userName1, setUserName1] = useState('')
    const [userName2, setUserName2] = useState('')
    const [userName3, setUserName3] = useState('')

    const [userPrice , setUserPrice] = useState([])


    const allMoneyHandler = (e) => {
        setAllMoney(e.target.value)
    }
    const userName1Handler = (e) => {
        setUserName1(e.target.value)
    }
    const userName2Handler = (e) => {
        setUserName2(e.target.value)
    }
    const userName3Handler = (e) => {
        setUserName3(e.target.value)
    }

    const answerMoney = () => {
        const random = Math.floor(Math.random() * 101)
        const random2 = Math.floor(Math.random() * (101 - random))
        const random3 = 100 - (random + random2)

        setUserPrice([Math.floor((random * 0.01) * allMoney),Math.floor((random2 * 0.01) * allMoney) ,Math.floor((random3 * 0.01) * allMoney)])
    }

    return (
        <div>
            <h2>N빵 금액 입력</h2>
            <input type={"text"} value={allMoney} onChange={allMoneyHandler}/>
            <h2>이름 입력</h2>
            <input type={"text"} value={userName1} onChange={userName1Handler}/>
            <input type={"text"} value={userName2} onChange={userName2Handler}/>
            <input type={"text"} value={userName3} onChange={userName3Handler}/>
            <button onClick={answerMoney}>복불복 N빵</button>
            <p>{userName1} : {userPrice[0]}</p>
            <p>{userName2} : {userPrice[1]}</p>
            <p>{userName3} : {userPrice[2]}</p>
        </div>
    )
}

export default Test4