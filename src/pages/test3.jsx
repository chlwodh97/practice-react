import React, {useState} from 'react';

const Test3 = () =>{
    const INIT_PRICE = 50000
    const BOX_PRICE = 5000

    const [money, setMoney] = useState(INIT_PRICE);
    const [history , setHistory] = useState([]);



    const goods = [
        {
            goodsName : "티코", goodsPercent : 0.01
        },
        {
            goodsName : "대화형 GPT", goodsPercent : 0.1
        },
        {
            goodsName : "나뭇가지", goodsPercent : 0.5
        },
        {
            goodsName : "몽둥이", goodsPercent : 0.1
        },
        {
            goodsName : "대나무 헬리콥터", goodsPercent : 0.09
        },
        {
            goodsName : "NCS 학습모듈교재 B", goodsPercent : 0.1
        },
        {
            goodsName : "DO it! 안드로이드 웹 프로그래밍", goodsPercent : 0.1
        },
    ]


    // 확률 구간에 해당하면 리턴시키기
    // 확률 설정할 때 많이 사용하는 기법이다
    const getRandomGoods = () => {
        const random = Math.random() //0.2135321
        let sum = 0
        for (const good of goods) {
            sum += good.goodsPercent
            if (random < sum) return good
        }
    }


    const resetGame = () => {
        setMoney(INIT_PRICE)
        setHistory([])
    }

    const openBox = () => {
        // 내 돈이 최소박스보다 낮으면 나가
        if(money < BOX_PRICE) return false
        const good = getRandomGoods()
        setMoney(money - BOX_PRICE)
        setHistory([...history, good.goodsName])

    }

    return (
        <div>
            <p> 잔약 : {money}원 </p>
            <button onClick={openBox}>랜덤박스 열기</button>
            <button onClick={resetGame}>다시하기</button>
            <h3>당첨 기록</h3>
            <ul>
                {history.map((item, i) => (
                    <li key={i}>{item}</li>
                ))}
            </ul>
        </div>
    )
}

export default Test3