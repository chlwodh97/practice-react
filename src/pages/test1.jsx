import React, {useState} from 'react';

const Test1 = () =>{
    const [manName, setNamName] = useState('');
    const [girlName, setGirlName] = useState('');
    const [score , setScore] = useState(0);


    const handleSubmit = (e) => {
        e.preventDefault()
        setScore(Math.floor(Math.random() * 101));
    }


    const handleMan = (e) => {
        setNamName(e.target.value)
    }

    const handleGirl = (e) => {
        setGirlName(e.target.value);
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <input value={manName} onChange={handleMan}/>
                <input value={girlName} onChange={handleGirl}/>
                <button type="submit">궁합보기</button>
            </form>
            <div>
                {manName}님과 {girlName}의 궁합도는 {score}% 임다
            </div>
        </div>
    )
}

export default Test1