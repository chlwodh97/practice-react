import React, {useState} from 'react';

const Test2 = () =>{
    const [userTop , setUserTop] = useState('');
    const [userPants , setUserPants] = useState('');
    const [pickTop, setPickTop] = useState('');
    const [pickPant, setPickPant] = useState('');


    const userTopHandler = (e) => {
        setUserTop(e.target.value)
    }

    const userPantsHandler = (e) => {
        setUserPants(e.target.value)
    }

    const pickTopPants = () => {
        const topArr = userTop.split(',')
        const pantsArr = userPants.split(',')

        setPickTop(topArr[Math.floor(Math.random() * topArr.length)])
        setPickPant(pantsArr[Math.floor(Math.random() * pantsArr.length)])
    }

    return (
        <div>
            <div>
                상의 : <input type={"text"} value={userTop} onChange={userTopHandler}/> <br/>
                하의 : <input type={"text"} value={userPants} onChange={userPantsHandler}/> <br/>
                <button onClick={pickTopPants}>옷추천하기</button>
                <p>내일 상의는 {pickTop} 하의는 {pickPant}을 입으삼</p>
            </div>
        </div>
    )
}

export default Test2