import './App.css';
import Test1 from "./pages/test1";
import {Route, Routes} from "react-router-dom";
import DefaultLayout from "./layouts/DefaultLayout";
import Test2 from "./pages/test2";
import Test3 from "./pages/test3";
import Test4 from "./pages/test4";

function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<DefaultLayout><Test1/></DefaultLayout>}></Route>
            <Route path="/t2" element={<DefaultLayout><Test2/></DefaultLayout>}></Route>
            <Route path="/t3" element={<DefaultLayout><Test3/></DefaultLayout>}></Route>
            <Route path="/t4" element={<DefaultLayout><Test4/></DefaultLayout>}></Route>
        </Routes>
    </div>
  );
}

export default App;
